var login = angular.module('invento-login', ['stripe']);

/**
 * There are a few ENV variables that must be present in the env.js file as constant under the ENV namespace
 * XDSTORAGE a url to the XD localstorage
 * LHQ_MARKETING_URL for the register button
 * LHQ_API_URL This is the route to the LHQ API
 * API_KEY The key for the app making the request
 */

/**
 * We need some routing for the login form, this needs to be in 'views/login/login.html'
 */
login.config(function($routeProvider){

    $routeProvider.when('/login',{
        templateUrl : 'bower_components/invento-login/dist/html/login.html',
        controller  : 'inventoLogin'
    });

    $routeProvider.when('/logout',{
        templateUrl : 'bower_components/invento-login/dist/html/login.html',
        controller  : 'inventoLogout'
    });

});

/**
 * We need to initiate the cross domain local storage before the page loads in a run block
 */
login.run(function(ENV, xdLocalStorage, CrossStorage, $rootScope, $location, $window, $localStorage, $route) {
    var curUrl = new URI($window.location.href).host();

    Stripe.setPublishableKey(ENV.STRIPE_KEY);

    // console.log('login run block');
    xdLocalStorage.init({
        iframeUrl : ENV.XDSTORAGE
    }).then(function(){
        // console.log('XD init');
        var retreived = {};

        // Set variables for XD promises
        var destroyed = xdLocalStorage.getItem('destroyed');
        var user      = xdLocalStorage.getItem('user');
        var authed    = xdLocalStorage.getItem('authed');
        var admin     = xdLocalStorage.getItem('super');
        var company   = xdLocalStorage.getItem('company');
        var managed   = xdLocalStorage.getItem('managed');
        var manager   = xdLocalStorage.getItem('manager');

        destroyed.then(function(success){
            retreived.destroyed    = true;
            // Do a bit of work here to ensure the response is a boolean not a string
            if (success.value === null) {
                success.value = true;
            }else if (success.value.toLowerCase() === 'true') {
                success.value = true;
            } else if (success.value.toLowerCase() === 'false') {
                success.value = false;
            }
            CrossStorage.destroyed = success.value;
            XDLoaded(retreived);
        });

        user.then(function(success){
            retreived.user    = true;
            CrossStorage.user = success.value;
            XDLoaded(retreived);
        });

        authed.then(function(success){
            retreived.authed    = true;
            CrossStorage.authed = success.value == null ? null : parseInt(success.value);
            XDLoaded(retreived);
        });

        admin.then(function(success){
            retreived.admin    = true;
            // Do a bit of work here to ensure the response is a boolean not a string
            if (success.value === null) {
                success.value = false;
            }else if (success.value.toLowerCase() === 'true') {
                success.value = true;
            } else if (success.value.toLowerCase() === 'false') {
                success.value = false;
            }
            CrossStorage.super = success.value;
            XDLoaded(retreived);
        });

        company.then(function(success){
            retreived.company   = true;
            CrossStorage.company = success.value;
            XDLoaded(retreived);
        });

        managed.then(function(success) {
            retreived.managed = true;

            if (success.value === null) {
                success.value = false;
            } else {
                success.value = success.value.toLowerCase() === 'true';
            }

            CrossStorage.managed = success.value;
            XDLoaded(retreived);
        });

        manager.then(function(success) {
            retreived.manager = true;

            if (success.value === null) {
                success.value = false;
            } else {
                success.value = success.value.toLowerCase() === 'true';
            }

            CrossStorage.manager = success.value;
            XDLoaded(retreived);
        });
    });

    function XDLoaded(retreived) {
        if (retreived.destroyed && retreived.user && retreived.authed && retreived.admin && retreived.company && retreived.managed && retreived.manager) {
            // console.log('all XD retreived');
            CrossStorage.matchStorage();
            CrossStorage.loaded = true;

            if (CrossStorage.destroyed === true || !CrossStorage.access()) {
                // If the route is public we do not need to redirect them to login
                if (!$route || !$route.current || !$route.current.$$route || !$route.current.$$route.public) {
                    // Let's remember where they were trying to go so we can redirect them back after they log in
                    $localStorage.login_redirect = $location.path();

                    $location.path('login');
                }
            }

            $rootScope.$emit('XDLoaded');
        }
    }

});

/**
 * This factory is used to make the request to the API to validate the login details
 */
login.factory('Auth', function(ENV, $resource, $location, $localStorage){
    var auth = {};
    
    auth.login = $resource(ENV.LHQ_API_URL + "login", {}, {
        post : { 
            method  : "POST",
            headers : { 'api-key' : ENV.API_KEY }
        },
    });

    auth.subFromLogin = function(user) {
        return $resource(ENV.LHQ_API_URL + 'companies/from_login', {}, {
            post : {
                method  : 'POST',
                headers : {
                    'api-key'   : ENV.API_KEY,
                    'email'     : user.email,
                    'token'     : user.token
                }
            }
        });
    };

    /** Test to see if a user is logged in (uses the local storage only) */
    auth.check = function(){
        if (angular.isDefined($localStorage.user) && angular.isDefined($localStorage.authed) && angular.isDefined($localStorage.destroyed) && $localStorage.destroyed !== true) {
            return true;
        } else {
            return false;
        }
    };

    /** Test to see if the logged in user has an admin flag */
    auth.admin = function(){
        if (angular.isDefined($localStorage.user) && $localStorage.user.admin === 1) {
            return true;
        } else {
            return false;
        }
    };

    /** Test to see if the logged in user is a super admin */
    auth.super = function() {
        if (angular.isDefined($localStorage.user) && angular.isDefined($localStorage.super) && $localStorage.super === true) {
            return true;
        } else {
            return false;
        }
    };

    /** Gets the logged in user as a JS object */
    auth.user = function() {
        return angular.fromJson($localStorage.user);
    };

    /** If the user is logged in redirect them to the dashboard */
    auth.redirect = function(){
        if (this.check()) {
            $location.path('/dashboard');
        }
    };

    return auth;
});

/**
 * This factory holds the XD settings after init to be used later if needed
 */
login.factory('CrossStorage', function($localStorage, $rootScope, $window, ENV) {
    var storage = {};

    storage.user      = false;
    storage.authed    = false;
    storage.destroyed = false;
    storage.super     = false;
    storage.company   = false;
    storage.managed   = false;
    storage.loaded    = false;

    /** This function will determine which set of credentials are newer, localStorage or XD storage and set them both to match */
    storage.matchStorage = function(){
        // We should always have authed timestamp the most recent one therefore must be the most recent actions
        var localtime = $localStorage.authed ? $localStorage.authed : 0; // incase there isnt one
        var xdtime    = this.authed ? this.authed : 0; // incase there isnt one
        if(localtime === xdtime) {
            // console.log('Both timestamps are the same');
            // If both are the same it makes more sense for us to match the localstorage to the XD (in case)
            // If the XD is destroyed we need to destroy the local
            if (this.destroyed === true) {
                this.destroyLocal(this.authed);
            } else {
                this.updateLocal();
            }
        } else if(localtime > xdtime) {
            // console.log('local data is more recent');
            if ($localStorage.destroyed === true) {
                this.destroyXD($localStorage.authed);
                this.destroyFactory($localStorage.authed);
            } else {
                this.updateFactory();
                this.updateXD();
            }
        } else if(xdtime > localtime) {
            // console.log('XD data is more recent');
            if (this.destroyed === true) {
                this.destroyLocal(this.authed);
            } else {
                this.updateLocal();
            }
        }
    };

    /** This destroys the local storage as the XD is destroyed */
    storage.destroyLocal = function(authed){
        // console.log('Destroy the local');
        $localStorage.user      = false;
        $localStorage.authed    = authed;
        $localStorage.destroyed = true;
        $localStorage.company   = false;
        $localStorage.super     = false;
        $localStorage.managed   = false;
        $localStorage.manager   = false;

        $rootScope.XDFinished = true;
    };

    /** This will update the local storage */
    storage.updateLocal = function(){
        // console.log('Update local function');
        $localStorage.user      = angular.fromJson(this.user);
        $localStorage.authed    = this.authed;
        $localStorage.destroyed = this.destroyed;
        $localStorage.company   = this.company;
        $localStorage.super     = this.super;
        $localStorage.managed   = this.managed;
        $localStorage.manager   = this.manager;

        $rootScope.XDFinished = true;
    };

    /** This will update this factorys variables */
    storage.updateFactory = function(){
        // console.log('Update this factory');
        this.user      = $localStorage.user;
        this.authed    = $localStorage.authed;
        this.destroyed = $localStorage.destroyed;
        this.company   = $localStorage.company;
        this.super     = $localStorage.super;
        this.managed   = $localStorage.managed;
        this.manager   = $localStorage.manager;

        $rootScope.XDFinished = true;
    };

    /** This will actually update the XD storage AYSNC */
    storage.updateXD = function(){
        // console.log('Update XD storage');
        xdLocalStorage.setItem('user',angular.toJson($localStorage.user));
        xdLocalStorage.setItem('authed',$localStorage.authed);
        xdLocalStorage.setItem('destroyed',$localStorage.destroyed);
        xdLocalStorage.setItem('company',$localStorage.company);
        xdLocalStorage.setItem('super',$localStorage.super);
        xdLocalStorage.setItem('managed',$localStorage.managed);
        xdLocalStorage.setItem('manager',$localStorage.manager);

        $rootScope.XDFinished = true;
    };

    /** This will destroy the factory */
    storage.destroyFactory = function(authed){
        // console.log('Destroy the factory');
        this.user      = false;
        this.authed    = authed;
        this.destroyed = true;
        this.company   = false;
        this.super     = false;
        this.managed   = false;
        this.manager   = false;

        $rootScope.XDFinished = true;
    };

    /** This will destroy the XD Async */
    storage.destroyXD = function(authed){
        // console.log('Destroy the XD');
        xdLocalStorage.setItem('destroyed', true);
        xdLocalStorage.setItem('user', false);
        xdLocalStorage.setItem('authed', authed);
        xdLocalStorage.setItem('company', false);
        xdLocalStorage.setItem('super', false);
        xdLocalStorage.setItem('managed', false);
        xdLocalStorage.setItem('manager', false);

        $rootScope.XDFinished = true;
    };

    // Determine whether the details provided mean they should have access to the product
    storage.access = function() {
        var curUrl = new URI($window.location.href).host();

        if (typeof ENV.APP_NAME !== 'undefined' && ENV.APP_NAME.toLowerCase() === 'lettinghq' && this.managed) {
            return false;
        } else if (curUrl.indexOf(ENV.APP_BASE_URL) === -1 && !this.managed && !this.manager && !this.super && !this.destroyed) {
            return false;
        } else if (curUrl.indexOf(ENV.APP_BASE_URL) > -1) {
            if (this.managed) {
                return false;
            } else if (this.manager && (typeof ENV.APP_NAME === 'undefined' && ENV.APP_NAME.toLowerCase() !== 'lettinghq')) {
                return false;
            }
        }

        return true;
    }

    return storage;
});

login.controller('inventoLogin', function($scope, $rootScope, $localStorage, $location, $timeout, $window, ENV, Auth, ResponseMessage, CrossStorage){
    /** We send the link for the marketing page to the view */
    $scope.lhqMarketingLink = ENV.LHQ_MARKETING_URL;
    $scope.XDLoaded         = true;

    if ($localStorage.access_attempt_response) {
        $localStorage.user = false;

        $timeout(function() {
            $rootScope.$broadcast('LoginAccessRequired', $localStorage.access_attempt_response);
        }, 200);
    } else if (!CrossStorage.access()) {
        $localStorage.user = false;
    } else {
        Auth.redirect();
    }

    login = Auth.login;

    $scope.inventoLogin = function() {
        if (CrossStorage.destroyed === false && !CrossStorage.access()) {
            ResponseMessage.error('You do not have access to this product');
        } else {
            login.post({
                email    : $scope.loginEmail,
                password : $scope.loginPassword
            }, function(success){
                // console.log('logged in');
                var timestamp   = new Date().getTime(),
                    user        = success.response.data;

                // Set the local user and authed
                $localStorage.authed    = timestamp;
                $localStorage.destroyed = false;
                $localStorage.super     = user.super ? user.super : false;
                $localStorage.company   = user.managing_id ? user.managing_id : user.company_id;

                if (user.company != null && user.company.management_settings != null) {
                    if (user.company.management_settings.managed_by != null) {
                        $localStorage.managed = true;
                    } else {
                        $localStorage.managed = false;
                    }

                    if (user.company.management_settings.manager === 1) {
                        $localStorage.manager = true;
                    } else {
                        $localStorage.manager = false;
                    }
                } else {
                    $localStorage.manager = false;
                    $localStorage.managed = false;
                }

                if (user.company) {
                    delete user.company;
                }

                $localStorage.user = user;

                CrossStorage.matchStorage();

                // If there is a route they were trying to get to originally redirect them there, otherwise go to dashboard
                if (typeof $localStorage.login_redirect === 'string') {
                    var location = $localStorage.login_redirect;
                    delete $localStorage.login_redirect;

                    if (location.indexOf('login') === -1 && location.indexOf('logout') === -1) {
                        $location.path(location);
                    } else {
                        $location.path('/dashboard');
                    }
                } else {
                    $location.path('/dashboard');
                }
            }, function(error){
                if (error.status === 401) {
                    $rootScope.$broadcast('LoginAccessRequired', error.data.response.data);
                } else {
                    ResponseMessage.error(error, true);
                }
            });
        }
    };
});

login.controller('modals.login.pageless', function($scope, $rootScope, ResponseMessage, $localStorage, CrossStorage, Auth) {
    var login           = Auth.login;
    $scope.email        = null;
    $scope.password     = null;
    $scope.loggingIn    = false;

    $('#modal-login').on('show.bs.modal', function(event) {
        $scope.$evalAsync(function(scope) {
            scope.email        = null;
            scope.password     = null;
            scope.loggingIn    = false;
        });
    });

    $scope.login = function() {
        $scope.loggingIn = true;

        login.post({
            email    : $scope.email,
            password : $scope.password
        }, function(success){
            // console.log('logged in');
            var timestamp = new Date().getTime();
            // Set the local user and authed
            $localStorage.user      = success.response.data;
            $localStorage.authed    = timestamp;
            $localStorage.destroyed = false;
            $localStorage.super     = success.response.data.super ? success.response.data.super : false;
            $localStorage.company   = $localStorage.user.company_id;

            CrossStorage.matchStorage();
            $('#modal-login').modal('hide');
            
            $rootScope.$broadcast('UserLoggedIn');
        }, function(error) {
            $scope.loggingIn = false;
            
            ResponseMessage.error(error, true);
        });
    };
});

login.controller('modals.login.grantAccess', function($scope, $rootScope, ResponseMessage, $localStorage, $location, $timeout, CrossStorage, Auth) {
    $scope.plans        = [];
    $scope.selectedPlan = {};
    $scope.validation   = {};
    $scope.user_details = {
        email           : null,
        token           : null,
        admin           : false,
        subscription    : false
    };
    $scope.card = {
        number      : null,
        cvc         : null,
        expmonth    : null,
        expyear     : null,
        coupon      : null,
    };

    $scope.$on('LoginAccessRequired', function(event, data) {
        $scope.$evalAsync(function(scope) {
            scope.user_details.email           = data.user_attempt_email;
            scope.user_details.token           = data.user_attempt_auth_token;
            scope.user_details.admin           = data.user_attempt_admin === 1;
            scope.user_details.subscription    = data.current_active_stripe_subscription;
            scope.plans                        = data.possible_plans_for_product;

            $('#modals-login-grant-access').modal('show');
        });
    });

    $scope.subSuccess = function(success) {
        var timestamp = new Date().getTime();

        $localStorage.user      = success.response.data;
        $localStorage.authed    = timestamp;
        $localStorage.destroyed = false;
        $localStorage.super     = success.response.data.super ? success.response.data.super : false;
        $localStorage.company   = $localStorage.user.company_id;

        if ($localStorage.access_attempt_response) {
            delete $localStorage.access_attempt_response;
        }

        CrossStorage.matchStorage();
        $('#modals-login-grant-access').modal('hide');

        $timeout(function() {
            $location.path('/dashboard');
        }, 500);
    }

    $scope.activatePlan = function() {
        var fromLogin       = Auth.subFromLogin($scope.user_details),
            selectedPlan    = JSON.parse($('input[name=selected-plan]').val());

        fromLogin.post({
            plan_id : selectedPlan.stripe_plan_id,
        }, function(success) {
            $scope.subSuccess(success);
        }, function(error) {
            ResponseMessage.error(error, true);
        });
    };

    $scope.newSubscription = function(status, response) {
        var selectedPlan = JSON.parse($('input[name=selected-plan]').val());
        if (selectedPlan.expired_trial) {
            if (status === 400 || status === 402) {
                ResponseMessage.error(response.error.message);
                $scope.validation = {number : true, cvc : true, expmonth : true, expyear : true};

                if (!$scope.card) {
                    $scope.validation.number    = false;
                    $scope.validation.cvc       = false;
                    $scope.validation.expmonth  = false;
                    $scope.validation.expyear   = false;
                } else {
                    $scope.validation.number    = !!$scope.card.number;
                    $scope.validation.cvc       = !!$scope.card.cvc;
                    $scope.validation.expmonth  = !!$scope.card.expmonth;
                    $scope.validation.expyear   = !!$scope.card.expyear;
                }
            } else if (status === 200) {
                var fromLogin = Auth.subFromLogin($scope.user_details);

                fromLogin.post({
                    plan_id         : selectedPlan.stripe_plan_id,
                    stripe_token    : response.id,
                }, function(success) {
                    $scope.subSuccess(success);
                }, function(error) {
                    ResponseMessage.error(error, true);
                });
            }
        } else {
            var fromLogin = Auth.subFromLogin($scope.user_details);

            fromLogin.post({
                plan_id         : selectedPlan.stripe_plan_id
            }, function(success) {
                $scope.subSuccess(success);
            }, function(error) {
                ResponseMessage.error(error, true);
            });
        }
    };
});

login.controller('inventoLogout', function($rootScope, $localStorage, CrossStorage, $location){
    // console.log('logout controller called');
    var timestamp = new Date().getTime();
    CrossStorage.destroyLocal(timestamp);
    CrossStorage.matchStorage();
    $rootScope.$broadcast('EnableNotificationCheck');
    $location.path('/login');
});

login.controller('inventoLogoutPageless', function($scope, $rootScope, CrossStorage) {
    $scope.logout = function() {
        // console.log('Logging out without directing to a page');
        CrossStorage.destroyLocal(new Date().getTime());
        CrossStorage.matchStorage();
        // console.log('logged out');
        
        $rootScope.$broadcast('UserLoggedOut');
    };
});
